// Projet 4 - Digitaliation d'un super-marché
// INFO-F-105
// Header de la classe Store
// Noëmie Muller
// 000458865
// 22-05-2018

#ifndef Store_h
#define Store_h

#include "Aisle.hpp"

#include <string>
#include <vector>


class Store
{
    public:
    
    Store(std::string storeName, int nbAisle, int nbItem);
    /*
     * \brief   Constructeur de la classe Store
     * \param   String      Nom du magasin
     *          Int         Nombre de rayons du magasin
                Int         Nombre d'articles par rayon
     * \return  None
     */
    
    float getActifs() const;
    /*
     * \brief   Calcule les actifs du magasin, à savoir la valeur totale de tous les articles contenus
     * \param   None
     * \return  Float       Somme des actifs
     */
    
    void describe() const;
    /*
     * \brief   Décrit tout ce qu'il y a dans le magasin.
     * \param   None
     * \return  None
     */
    
    bool fullStore() const;
    /*
     * \brief   Indique si le magasin est rempli
     * \param   None
     * \return  Bool        True si le magasin est rempli
     */
    
    void receiveShipment(std::vector<std::pair<Item,int>> livraison);
    /*
     * \brief   Ajoute au magasin les articles reçus en paramètre et affiche un message d'erreur si il n'y a pas suffisamment d'espace
     * \param   Vector<pair<Item,int>>      Liste de tuples représentant chacun un article et la                                        quantité d'unités de celui-ci à ajouter au magasin
     * \return  None
     */
    
    void sell(std::vector<std::pair<Item,int>> vente); //parametres a préciser
    /*
     * \brief   Retire les articles vendus du magasin dans les bonnes quantités
     * \param   Vector<pair<Item,int>>      Liste de tuples représentant chacun un article et la                                        quantité d'unités de celui-ci à retirer du magasin
     * \return  None
     */
    
    void newAisle(int nbAisle);
    /*
     * \brief   Crée un nouveau rayon
     * \param   None                               
     * \return  None
     */

    Aisle* getAisle(int numberAisle);
    /*
     * \brief   Renvoie le pointeur vers le rayon correspondant au numéro donné
     * \param   Int     Numéro du rayon vers lequel on veut pointer
     * \return  Pointeur vers l'objet Aisle demandé
     */
    
    private :
    
    std::string m_storeName;
    //nom du magasin
    std::vector<Aisle*> m_rayons;
    //vecteur de pointeurs vers les rayons du magasin
    int m_nbAisle;
    //nombre de rayons du magasin
    int m_nbItem;
    //capacité des rayons du magasin
    
};
#endif
