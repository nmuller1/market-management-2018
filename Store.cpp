// Projet 4 - Digitaliation d'un super-marché
// INFO-F-105
// Code source de la classe Store
// Noëmie Muller
// 000458865
// 22-05-2018

#include "Store.hpp"

#include <string>
#include <vector>
#include <iostream>

using namespace std;

Store::Store(string storeName, int nbAisle, int nbItem) : m_storeName(storeName), m_nbAisle(nbAisle), m_nbItem(nbItem)
{
    newAisle(0);
    /*La non-initialisation du vecteur m_rayons dans la liste d'initialisation provoque un warning
    J'ai décidé de ne pas le faire car je trouvais plus pratique de créer à l'aide d'une fonction newAisle un premier rayon 0
     */
}

float Store::getActifs() const
{
    float count(0); //on initialise un compteur des actifs
    for(size_t i(0);i < m_rayons.size();i++) //on va parcourir tous les rayons du magasin
    {
        count += m_rayons[i]->getActifs(); //calcule les avoirs de chaque rayon et les somme
    }
    return count;
}

void Store::describe() const
{
    cout << "Il s'agit du magasin " << m_storeName << ". Il contient " << m_nbAisle << " rayons ayant une capacité de " << m_nbItem << " articles." << endl;
    if (!(m_rayons[0]->isEmpty()))
    {
        cout << "Voici l'inventaire du magasin :" << endl;
        int countItems(0);
        for (size_t i(0);i<m_rayons.size();i++)
        {
            m_rayons[i]->describe();
            countItems += m_rayons[i]->getStock();
            cout << "Le prix moyen d'un article de ce rayon est de " << m_rayons[i]->getActifs()/float(m_rayons[i]->getStock()) << "€." << endl;
        }
        cout << "Le magasin évalue ses actifs à " << getActifs() << "€" << endl;
        cout << "Le prix moyen des articles du magasin est évalué à " << (getActifs()/float(countItems)) << "€." << endl;;
    }
    else
    {
        cout << "Le magasin est encore vide." << endl;
    }
}

bool Store::fullStore() const
{
    return (int(m_rayons.size()) == m_nbAisle && m_rayons.back()->isFull());
    //le magasin est rempli si 20 rayons sont utilisés et que le dernier rayon est rempli"
}

void Store::receiveShipment(vector<pair<Item,int>> livraison)
{
    cout << "Une nouvelle livraison est arrivée." << endl;
    int currentAisle(0); //on initialise un compteur pour évoluer dans les rayons à remplir
    for (size_t article(0);article < livraison.size();article++) //on passe en revue chaque article à ajouter au stock
    {
        Item nameItem(livraison[article].first);
        int nbItemsLeft(livraison[article].second);
        while (nbItemsLeft > 0 && !(this->fullStore())) //on passe en revue chaque article à ajouter tant qu'il y a de la place dans le magasin
        {
            nbItemsLeft = getAisle(currentAisle)->add(nameItem,nbItemsLeft);
            if (nbItemsLeft > 0 && !(this->fullStore()))  //s'il reste des articles, c'est que le rayon dans lequel on se trouve est rempli donc si le magasin n'est pas rempli, on passe au rayon suivant
            {
                currentAisle += 1;
                newAisle(currentAisle);
            }
            else if (nbItemsLeft > 0 && this->fullStore())
            {
                cout << "Tous les éléments de la livraison n'ont pas pu être ajoutés au stock car le magasin est rempli. N'ont pas été ajoutés : " << endl;
            }
        }
        if (this->fullStore())
        {
            cout << nbItemsLeft << " " << nameItem.getName() << endl;
        }
    }
}

void Store::sell(vector<pair<Item,int>> vente)
{
    for (size_t article(0);article < vente.size();article++) //on passe en revue chaque article à vendre
    {
        Item nameItem(vente[article].first);
        int nbItemsLeft(vente[article].second);
        int currentAisle(0); //on initialise un compteur pour évoluer dans les rayons
        while (nbItemsLeft > 0 && currentAisle < int(m_rayons.size())) //on passe en revue chaque rayon jusqu'à ce que tous les articles aient été retirés ou qu'on soit arrivé au bout du magasin
        {
            nbItemsLeft = getAisle(currentAisle)->remove(nameItem,nbItemsLeft);
            if (getAisle(currentAisle)->isEmpty())
            {
                delete getAisle(currentAisle);
                m_rayons.erase(m_rayons.begin()+currentAisle);
            }
            else
            {
                currentAisle += 1 ;
            }
        }
        if (nbItemsLeft != 0)
        {
            cout << "Le magasin est en rupture de stock de " << nameItem.getName() << ". " << nbItemsLeft << " article(s) n'ont pas pu être vendus." << endl;
        }
    }
}

void Store::newAisle(int nbAisle)
{
    Aisle *pointeur(0); //on crée un pointeur pour un nouveau rayon
    /*Le pointeur nul provoque un warning mais il est préférable d'initialiser un pointeur à zéro car un pointeur sans adresse connue est une situation dangereuse. On risquerait d'utiliser ce pointeur en se trompant, sans savoir quelle partie de la mémoire on manipule.
     */
    pointeur = new Aisle(nbAisle,m_nbItem);//on crée un nouveau rayon et on y assigne le pointeur
    m_rayons.push_back(pointeur); //on ajoute le pointeur vers le nouveau rayon au vecteur
}

Aisle* Store::getAisle(int numberAisle)
{
    return m_rayons[size_t(numberAisle)];
}

        
