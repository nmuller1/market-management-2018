// Projet 4 - Digitaliation d'un super-marché
// INFO-F-105
// Fichier de tests
// Noëmie Muller
// 000458865
// 22-05-2018

#include "Store.hpp"

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{
    Store Carrefour("Carrefour",5,5);
    
    Carrefour.describe();
    
    Item pain("pain(s)",5),tomate("tomate(s)",1),kinder("kinder(s)",2),carotte("carotte(s)",10),baguette("baguette(s)",25),poulet("poulet(s)",15);
    vector<pair<Item,int>> livraison = { {pain,6}, {tomate,5},{kinder,6},{carotte,3},{baguette,3},{poulet,9}};
    
    Carrefour.receiveShipment(livraison);

    Carrefour.describe();

    if (Carrefour.fullStore())
    {
        cout << "Le magasin est rempli." << endl;
    }
    Item poivre("poivre",1);
    vector<pair<Item,int>> vente = { {pain,3}, {tomate,5},{kinder,1},{carotte,8},{poivre,13},{poulet,18}};
    Carrefour.sell(vente);
    
    return 0;
}




