// Projet 4 - Digitaliation d'un super-marché
// INFO-F-105
// Code source de la classe Aisle
// Noëmie Muller
// 000458865
// 22-05-2018

#include "Aisle.hpp"

#include <string>
#include <vector>
#include <iostream>

using namespace std;

Aisle::Aisle(int number, int capacityAisle) : m_number(number), m_capacity(capacityAisle), m_stock((0))
{
    /*La non-initialisation du vecteur m_contents dans la liste d'initialisation provoque un warning
     Celui-ci est laissé non-initialisé lors de la construction car il est vide jusqu'à ce que des Items soient ajoutés au rayon.
     */
}

void Aisle::describe() const
{
    if (m_stock == 0)
    {
        cout << "Le rayon numéro " << m_number << " est vide." << endl;
    }
    else
    {
        cout << "Voici le contenu du rayon numéro " << m_number+1 << " : " << endl;
        for(size_t i(0);i < m_contents.size();i++)
        {
            string nameItem(m_contents[i].getName());
            float priceItem(m_contents[i].getPrice());
            int quantityItem(m_contents[i].getQuantity());
            //on récupère toutes les valeurs à imprimer
            if (quantityItem > 1)
            {
                nameItem+='s'; //mise au pluriel si nécessaire
            }
            cout << quantityItem << nameItem << " au prix de " << priceItem << "€" << endl;
        }
    }
}

size_t Aisle::findItem(Item &nameItem) const
{
    size_t i(0);
    bool found(false);
    while (i < m_contents.size() && !(found)) //check si l'article est déjà en rayon
    {
        found = (m_contents[i].getName()==nameItem.getName());
        if (!(found))
        {
            i += 1;
        }
    }
    return i; //renvoie l'index de l'article si trouvé ou le nombre d'éléments du vecteur
}

int Aisle::add(Item nameItem, int nbItem)
{
    int nbItemsLeft(m_stock + nbItem - m_capacity); //on calcule le nombre d'unités de l'article qui resteront à placer après l'ajout d'un maximum dans ce rayon
    if (nbItemsLeft < 0)
    {
        nbItemsLeft = 0;
    }
    nbItem -= nbItemsLeft; //on recalcule le nombre d'unités de l'article a ajouter dans ce rayon dans la mesure de sa capacité
    if (!(nbItem==0)) //check si on a au moins la place pour ajouter un article dans le rayon
    {
        size_t index(findItem(nameItem));
        if (index != m_contents.size()) //si l'article est déjà dans le rayon, l'index renvoie à l'indice de l'article dans le vecteur
        {
            m_contents[index].setQuantity(nbItem); //on ajoute la quantité demandée (dans la mesure de la capacité du rayon) à l'article
            m_stock += nbItem;
        }
        else //l'article n'est pas encore en rayon, il faut le rajouter
        {
            nameItem.setQuantity(nbItem);
            m_contents.push_back(nameItem);
            m_stock += nbItem;
        }
    }
    return nbItemsLeft;
}

int Aisle::remove(Item nameItem, int nbItem)
{
    int nbItemsLeft;
    size_t index(findItem(nameItem));
    if (index != m_contents.size()) //check si l'article à retirer est bien dans le rayon
    {
        nbItemsLeft = nbItem - m_contents[index].getQuantity(); //on calcule le nombre d'unités de l'article qui seront encore à retirer après la soustraction d'un maximum dans ce rayon
        if (nbItemsLeft < 0)
        {
            nbItemsLeft = 0;
        }
        nbItem -= nbItemsLeft; //on recalcule le nombre d'unités que l'on peut retirer du rayon dans la mesure de son stock
        m_contents[index].setQuantity(- nbItem); //on retire le nombre d'unités demandées de l'article
        m_stock -= nbItem;
    }
    else
    {
        nbItemsLeft = nbItem; //si l'article n'est pas dans le rayon, on renvoie le nombre d'unités à retirer de base, qu'il faudra retirer dans d'autres rayons
    }
    return nbItemsLeft;
}

int Aisle::getNumber() const
{
    return m_number;
}

float Aisle::getActifs() const
{
    float count(0); //initialise un compteur des avoirs du rayon
    for(size_t i(0);i < m_contents.size();i++)
    {
        count += (m_contents[i].getPrice()*float(m_contents[i].getQuantity()));
    }
    return count;
}

bool Aisle::isFull()
{
    return (m_stock == m_capacity);
}

bool Aisle::isEmpty()
{
    return (m_stock == 0);
}

int Aisle::getStock()
{
    return m_stock;
}
