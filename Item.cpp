// Projet 4 - Digitaliation d'un super-marché
// INFO-F-105
// Code source de la classe Item
// Noëmie Muller
// 000458865
// 22-05-2018

#include "Item.hpp"

#include <string>

using namespace std;

Item::Item(string nameItem, float priceItem) : m_nameItem(nameItem), m_priceItem(priceItem), m_quantity(0)
{
}

string Item::getName() const
{
    return m_nameItem;
}

float Item::getPrice() const
{
    return m_priceItem;
}

int Item::getQuantity() const
{
    return m_quantity;
}

void Item::setQuantity(int nbItem)
{
    m_quantity += nbItem;
    if (m_quantity < 0)
    {
        m_quantity = 0;
    }
}

