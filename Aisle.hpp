// Projet 4 - Digitaliation d'un super-marché
// INFO-F-105
// Header de la classe Aisle
// Noëmie Muller
// 000458865
// 22-05-2018

#ifndef Aisle_h
#define Aisle_h

#include "Item.hpp"

#include <string>
#include <vector>


class Aisle
{
    public:
    
    Aisle(int number, int capacityAisle);
    /*
     * \brief  Constructeur de la claisse Aisle
     * \param  Int      Numéro du rayon (pour se repérer dans le magasin)
     *         Int      Capacité du rayon
     * \return None
     */
    
    void describe() const;
    /*
     * \brief  Affiche le contenu du rayon
     * \param  None
     * \return None
     */
    
    size_t findItem(Item &nameItem) const;
    /*
     * \brief  Parcoure le vecteur reprenant tous les articles du rayon pour trouver l'article demandé
     * \param  Item     Article recherché
     * \return Int      Index de l'article dans le vecteur s'il existe ou taille du vecteur si article inexistant
     */
    
    int add(Item nameItem, int nbItem);
    /*
     * \brief   Ajoute au rayon le nombre d'articles demandé dans la mesure de sa capacité
     * \param   Item      Article à ajouter
     *          Int       Nombre d'unités de l'article à ajouer
     * \return  Int       Nombre d'unités restantes à ajouter au magasin (si rayon rempli)
     */
    
    int remove(Item nameItem, int nbItem);
    /*
     * \brief   Retire du rayon le nombre d'articles demandé dans la mesure de son stock
     * \param   Item      Article à retirer
     *          Int       Nombre d'unités de l'article à retirer
     * \return  Int       Nombre d'unités restantes à retirer du magasin (si plus d'article de ce type dans le rayon)
     */

    int getNumber() const;
    /*
     * \brief   Renvoie le numéro du rayon
     * \param   None
     * \return  Int       Numéro du rayon
     */
    
    float getActifs() const;
    /*
     * \brief   Calcule les actifs du rayon, à savoir la valeur totale de tous les articles contenus
     * \param   None
     * \return  Float       Somme des actifs
     */
    
    bool isFull();
    /*
     * \brief   Indique si le rayon est rempli ou non
     * \param   None
     * \return  Bool     True si le rayon est rempli
     */
    
    bool isEmpty();
    /*
     * \brief   Indique si le rayon est vide ou non
     * \param   None
     * \return  Bool     True si le rayon est vide
     */
    
    int getStock();
    /*
     * \brief   Indique le nombre d'articles dans le rayon
     * \param   None
     * \return  Int     Nombre d'articles dans le rayon
     */
    
    
    private :
    
    int m_number ;
    //numéro du rayon
    int m_capacity ;
    //capacité du rayon
    int m_stock ;
    //nombre d'articles contenus dans le rayon
    std::vector<Item> m_contents;
    //vecteur reprenant tous les articles présents dans le rayon
};

#endif


