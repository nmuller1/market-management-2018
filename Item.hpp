// Projet 4 - Digitaliation d'un super-marché
// INFO-F-105
// Header de la classe Item
// Noëmie Muller
// 000458865
// 22-05-2018

#ifndef Item_h
#define Item_h

#include <string>

class Item
{
    public:
    
    Item(std::string nameItem, float priceItem);
    /*
     * \brief  Constructeur de la classe Item
     * \param  String   Nom de l'article
     *         Int      Prix de l'article
     * \return None
     */
    
    std::string getName() const;
    /*
     * \brief  Accesseur permettant de récupérer le nom de l'article
     * \param  None
     * \return String   Nom de l'article
     */
    
    float getPrice() const;
    /*
     * \brief  Accesseur permettant de récupérer le prix de l'article
     * \param  None
     * \return Float    Prix de l'article
     */
    
    int getQuantity() const;
    /*
     * \brief  Accesseur permettant de récupérer le nombre d'unités de l'article dans le rayon
     * \param  None
     * \return Int      Nombre d'unités de l'article dans le rayon
     */
    
    void setQuantity(int nbItem);
    /*
     * \brief  Accesseur permettant d'ajouter ou de retirer des unités de l'article à son compteur
     * \param  Int      Nombre d'unités à ajouter/retirer
     * \return None
     */
    
    private :
    
    std::string m_nameItem;
    //nom de l'article
    float m_priceItem;
    //prix de l'article
    int m_quantity;
    //nombre d'unités de l'article dans le rayon
};

#endif
